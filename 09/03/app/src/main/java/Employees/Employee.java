package Employees;
public class Employee {
    protected String name;
    protected String position;
    protected int salary;

    public Employee(String name, int salary, String position) {
        this.name = name;
        this.salary = salary;
        this.position = position;
    }

    public String getName(){
      return this.name;
    }
  
    public void setName(String name){
      this.name = name;
    }
  
    public String getPosition(){
      return this.position;
    }
  
    public void setPosition(String position){
      this.position = position;
    }
  
    public double getSalary(){
      return this.salary;
    }
  
    public void setSalary(int salary){
      this.salary = salary;
    }
  
    public int hashCode() {
        return name.length() * 100 + position.length() * 10 + salary;
    }
  
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean equals(Object obj) {
      if (this == obj){
          return true;
      }
      if (obj == null || getClass() != obj.getClass()){
          return false;
      }
      Employee obj1 = (Employee) obj;
    
      if (this.salary != obj1.salary){
          return false;
      }
      if (this.position != obj1.position){
          return false;
      }
      return this.name.equals(obj1.name);
    }

  
  
}
  
