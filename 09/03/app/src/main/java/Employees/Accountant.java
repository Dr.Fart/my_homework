package Employees;
import java.util.Objects;

public class Accountant extends Employee {
    protected String department;

    public Accountant(String name, int salary, String position) {
        super(name, salary, position);
        
    }

    public double giveMoney(double money) {
        return money;
    }

    @Override
    public String toString() {
        return "{" +
                " имя='" + name + '\'' +
                ", должность='" + position + '\'' +
                ", зарплата=" + salary +
                '}';
    }

    public String accountant(){  //характерный метод
        return "Я бухгалтер";
      }
    

}
