package Employees;
import java.util.Objects;

public class Engineer extends Employee {


    public Engineer(String name, int salary, String position){
        super(name, salary, position);
        
    }
    public String toString(){
        return "{" +
        "имя='" + name + '\'' +
        ", должность='" + position + '\'' +
        ", зарплата=" + salary +
        '}';
    }
    public String engineer(){  //характерный метод
        return "Я инженер";
    }
}
    