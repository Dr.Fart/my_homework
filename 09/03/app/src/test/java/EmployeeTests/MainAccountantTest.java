package EmployeeTests;
import org.junit.jupiter.api.Test;

import Employees.Accountant;
import Employees.MainAccountant;

import static org.junit.jupiter.api.Assertions.*;

class MainAccountantTest {
    @Test
    void test() {
        Accountant accountant = new Accountant("Vector", 120, "Главный бухгалтер");
        MainAccountant mainAccountant = new MainAccountant("Vik", 120, "Главный бухгалтер");
        assertNotEquals(new MainAccountant("Vector", 120, "Главный бухгалтер"), new MainAccountant("Vector", 120, "Главный бухгалтер"));
        assertEquals(new MainAccountant("Vector", 120, "Главный бухгалтер"), new MainAccountant("Vik", 120, "Главный бухгалтер"));
    }
}