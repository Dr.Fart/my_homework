package EmployeeTests;
import org.junit.jupiter.api.Test;

import Employees.Engineer;

import static org.junit.jupiter.api.Assertions.*;

class EngineerTest {
    @Test
    void test() {
        new Engineer("Roma", 10, "Инженер");
        assertEquals(new Engineer("Django", 1, "Инженер"), new Engineer("Django", 1, "Инженер"));
        assertNotEquals(new Engineer("Wall-e", 2000, "Инженер"), new Engineer("Gru", 10, "Инженер"));

    }

}