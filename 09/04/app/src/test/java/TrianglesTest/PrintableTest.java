package TrianglesTest;
import org.junit.jupiter.api.Test;

import Triangles.ObjectOperation;
import Triangles.Polygon;
import Triangles.Printable;
import Triangles.Text;
import Triangles.Triangle;

import static org.junit.jupiter.api.Assertions.*;

class PrintableTest {

    @Test
    void print() {
        Printable[] objects = {
                new Polygon(4, 4),
                new Polygon(5, 5),
                new Triangle(3),
                new Text("Плыли дни...", "Меланхолический"),
                new Text("Опять двойка", "Грустный")
        };

        ObjectOperation objectOperation = new ObjectOperation();
        objectOperation.show(objects);
    }
}