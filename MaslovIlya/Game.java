public class Game {

    public static void main(String[] args){
        Hero[] heroes = new Hero[10];
        heroes[0] = new Warrior();
        heroes[1] = new Warrior();
        heroes[2] = new Spearman();
        heroes[3] = new Spearman();
        heroes[4] = new Archer();
        heroes[5] = new Archer();
        heroes[6] = new Magician();
        heroes[7] = new Magician();
        heroes[8] = new Warrior();
        heroes[9] = new Warrior();

        int sumHealth = 0;
        int i = 0;
        do {
            sumHealth = sumHealth + heroes[i].health;
            i++;
        } while (i < 10);
        System.out.println(sumHealth);
        i = 0;
        do {
            i++;
        } while (i < 10);
    }
}

