package Prog;

public class FibonucciRec {
    public static int fibRec(int n, int[] arr){
        if (arr[n - 1] != 0){
            return arr[n - 1];
        }
        if (n <= 2){
            arr[n - 1] = n - 1;
            return arr[n - 1];
        }
        int result = fibRec(n - 1, arr) + fibRec(n - 2, arr);
        arr[n - 1] = result;
        return result;
       }

    public static  int[] getFib(int n){
        if (n == 0){
          return null;
        }
        int[] arrNew = new int[n];
        fibRec(n, arrNew);
        return arrNew;
    }

}